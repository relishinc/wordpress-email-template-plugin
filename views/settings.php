<div class="wrap">
	
	<h1 class="wp-heading-inline">Email templates settings</h1>
	<hr class="wp-header-end">

	<?php if ( isset($_GET['success']) && $_GET['success'] ): ?>
	<div class="notice notice-success is-dismissible">
	  <p>Settings saved.</p>
	</div>
	<?php endif; ?>
    	
	<form method="post" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" novalidate="novalidate">
		
		<input type="hidden" name="action" value="email_templates_save_settings">
		
		<div class="email-template-form">
		
  		<table class="form-table">
  			<tbody>
  				<tr>
    				<th scope="row">
      				<label for="upload_image_button">Email header image</label>
    				</th>
    				<td>
      				<?php wp_enqueue_media(); ?>
              <div class="image-preview-wrapper">
                <?php $image = wp_get_attachment_image_src( EmailTemplatesPlugin::getSetting('header_image_attachment_id'), 'email_header' ); ?>
                <img id="image-preview" src="<?php echo $image ? $image[0] : ''; ?>">
              </div>
              <input id="upload_image_button" type="button" class="button" value="<?php _e( 'Upload image' ); ?>" />
              <input type="hidden" name="email_template[header_image_attachment_id]" id="image_attachment_id" value="<?php echo EmailTemplatesPlugin::getSetting('header_image_attachment_id'); ?>">
    				</td>
  				</tr>
  				<tr>
  					<th scope="row">
  						<label for="email_header_content">Header content</label>
  					</th>
  					<td>
  						<textarea name="email_template[header_content]" id="email_header_content" class="regular-text code" rows="4"><?php echo EmailTemplatesPlugin::getSetting('header_content'); ?></textarea>
  						<p class="description">Global "header" stuff that sits above the message content</p>
  					</td>
  				</tr>
  				<tr>
  					<th scope="row">
  						<label for="email_footer_content">Footer content</label>
  					</th>
  					<td>
  						<textarea name="email_template[footer_content]" id="email_footer_content" class="regular-text code" rows="4"><?php echo EmailTemplatesPlugin::getSetting('footer_content'); ?></textarea>
  						<p class="description">Global "footer" stuff that sits below the message content</p>
  					</td>
  				</tr>
  				<tr>
  					<th scope="row">
  						<label for="email_footer_copyright">Copyright</label>
  					</th>
  					<td>
  						<input type="text" name="email_template[footer_copyright]" id="email_footer_copyright" class="regular-text ltr" value="<?php echo EmailTemplatesPlugin::getSetting('footer_copyright', 'Copyright ' . date('Y') . ' ' . get_bloginfo('name') ); ?>">
  					</td>
  				</tr>
  				<tr>
  					<th scope="row">
  						<label for="email_custom_css">Custom CSS</label>
  					</th>
  					<td>
              <textarea name="email_template[custom_css]" id="email_custom_css" class="regular-text code" rows="10"><?php echo EmailTemplatesPlugin::getSetting('custom_css'); ?></textarea>
              <p class="description">The HTML on the right will help you craft your selectors</p>
  					</td>
  				</tr>
  				<tr>
  					<th scope="row">
  						<label for="email_color_bg">Background color</label>
  					</th>
  					<td>
  						<input type="text" name="email_template[color_bg]" id="email_color_bg" class="color-field" value="<?php echo EmailTemplatesPlugin::getSetting('color_bg'); ?>">
  					</td>
  				</tr>						
  				<tr>
  					<th scope="row">
  						<label for="email_color_copyright">Copyright text</label>
  					</th>
  					<td>
  						<input type="text" name="email_template[color_copyright]" id="email_color_copyright" class="color-field" value="<?php echo EmailTemplatesPlugin::getSetting('color_copyright'); ?>">
  					</td>
  				</tr>						
  			</tbody>
  		</table>
  		
  		<div class="email-template-html">
    		<textarea readonly="true"><?php echo file_get_contents( plugin_dir_path( __FILE__ ) . '../templates/generic.html'); ?></textarea>
  		</div>
		
		</div>
		
		<p><a href="admin.php?page=email_templates_preview" target="_blank">Preview your email template</a>
		
		
		<p class="submit">
			<input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes">
		</p>

	</form>
		
</div><!-- wrap -->