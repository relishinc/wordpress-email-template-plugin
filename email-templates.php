<?php 
/**
 * Plugin Name: Email template
 * Plugin URI: https://relishinc.bitbucket.io
 * Description: Provides a super swank template for all your WP-generated emails
 * Version: 0.0.3
 * Author: Steve Palmer / Relish
 * Author URI: https://reli.sh
 * License: GPLv2
 */
 
require_once __DIR__ . '/vendor/autoload.php'; 

use Pelago\Emogrifier\CssInliner;

add_action( 'init', [ 'EmailTemplatesPlugin', 'init' ], 0 );

class EmailTemplatesPlugin {

	private static $namespace = 'relish-email-templates';
	private static $version = '0.1';
	  
  private static $defaults = [];

  public static function init()
  {
    $class = __CLASS__;
    new $class;
  }

  public function __construct() 
  {
    self::$defaults = [
      // text
      'message' => '
        <h1>A message from us</h1>
        <h2>We are back</h2>
        <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus</p>
        <ul>
           <li>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</li>
           <li>Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus.</li>
           <li>Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</li>
           <li>Pellentesque fermentum dolor. Aliquam quam lectus, facilisis auctor, ultrices ut, elementum vulputate, nunc.</li>
        </ul>
      ',
      'header_content' => '',
      'footer_content' => '',
      'footer_copyright' => 'Copyright ' . date('Y') . ' ' . get_bloginfo('name'),
      // colors
      'color_bg' => '#006aaa',
      'color_copyright' => '#62a0c5',
    ];
    
    add_image_size( 'email_header', 1200, 400 );
    
    add_action( 'wp_mail', [ $this, 'filterMail' ] );
    add_action( 'admin_menu', [ &$this, 'registerPages' ] );
    add_action( 'admin_post_email_templates_save_settings', [ &$this, 'saveSettings' ] );
    add_action( 'admin_enqueue_scripts', [ &$this, 'enqueueScripts' ] );
    
    // make sure password reset emails work
    add_filter( 'retrieve_password_message', [ $this, 'formatPlainTextMessage' ], 10 );
    
  }

  public function enqueueScripts()
  {
	  // scripts
		wp_enqueue_script( self::$namespace . '-admin', plugins_url( 'assets/js/admin.js', __FILE__ ), [ 'jquery', 'wp-color-picker' ], self::$version, TRUE );
		
		// styles
		wp_enqueue_style( self::$namespace . '-admin', plugins_url( 'assets/css/admin.css', __FILE__ ), [ 'wp-color-picker' ], self::$version, 'all' );
  }

  public function registerPages()
  {
	  // parent menu item
    $this->dashboard_page = $dashboard_page = add_menu_page(
         __( 'Email Template', 'textdomain' )
        ,__( 'Email Template', 'textdomain' )
        ,'read'
        ,'email_templates_settings'
        ,[ &$this, 'renderSettingsPage' ]
        ,'dashicons-email'
    );

    // child: settings
    add_submenu_page(
	    	'email_templates_settings'
        ,__( 'Settings', 'textdomain' )
        ,__( 'Settings', 'textdomain' )
        ,'read'
        ,'email_templates_settings'
        ,[ &$this, 'renderSettingsPage' ]
    );  
    
    // child: settings
    add_submenu_page(
	    	'email_templates_settings'
        ,__( 'Preview', 'textdomain' )
        ,__( 'Preview', 'textdomain' )
        ,'read'
        ,'email_templates_preview'
        ,[ &$this, 'renderPreviewPage' ]
    );    
        
  }

  public function renderSettingsPage() 
  {
	  wp_add_inline_script(
	    $this->namespace . '-main',
	    '
	      /*
        SETTINGS.init({
	        siteUrl:  "' . get_bloginfo('url') . '",
	        ajaxUrl:  "' . admin_url( 'admin-ajax.php' ) . '"
	      });
	      */
	    '
	  );	 	  
	  include( dirname( __FILE__ ) . '/views/settings.php' );
  } 
  
  public function renderPreviewPage() 
  {
    echo '
      <div class="wrap">
        <h1>Email template preview</h1>
        <h2>On wider screens</h2>
        <iframe class="email-desktop-preview" srcdoc="' . htmlspecialchars($this->previewTemplate(self::$defaults['message'])) . '"></iframe>
        <h2>On smaller screens</h2>
        <iframe class="email-mobile-preview" srcdoc="' . htmlspecialchars($this->previewTemplate(self::$defaults['message'])) . '"></iframe>
      </div>
    ';
  }     

  public function saveSettings()
  {
    update_option( self::$namespace . '_settings', $_POST['email_template'] );
	  wp_redirect(admin_url('admin.php?page=email_templates_settings&success=1'));
  }
  
  static function getSetting( $setting_id, $default = false )
  {
    $settings = get_option( self::$namespace . '_settings' );
    
    if ( isset( $settings[$setting_id] ) )
    {
      return $settings[$setting_id];
    }
    
    if ( isset( self::$defaults[$setting_id] ) )
    {
      return self::$defaults[$setting_id];
    }    
    
	  return $default;
  }
  
  public function doMustacheParsing( $subject, $vars )
  {
    preg_match_all('/\{\{\s*.+?\s*\}\}/', $subject, $matches);
  	foreach($matches[0] as $match)
  	{
  		$var = str_replace('{', '', str_replace('}', '', preg_replace('/\s+/', '', $match)));
  		
  		if( isset($vars[$var]) && ! is_array($vars[$var]) )
  		{
  			$subject = str_replace($match, $vars[$var], $subject);
  		}
  	}
  	
  	// remove any unreplaced vars
  	
  	return preg_replace('/\{\{\s*.+?\s*\}\}/', '', $subject);    
  }
  
  public function formatPlainTextMessage( $message )
  {
    $message = str_replace('<', '', $message);
    $message = str_replace('>', '', $message);
    $message = str_replace("\n", '<br>', $message);

    return $message;	  
  }

  public function filterMail( $args ) 
  {
    // ensure mail is being sent as html
    
    if ( isset($args['headers']) && is_array($args['headers']) )
    {
      $args['headers'][] = 'Content-Type: text/html; charset=UTF-8';
    }
    else
    {
      $args['headers'] = [ 'Content-Type: text/html; charset=UTF-8' ];
    }
    
    $args['message'] = $this->generateMailMessage($args['message']);
        
    return $args; 
  }
  
  public function generateMailMessage( $message )
  {    
    $html = file_get_contents( plugin_dir_path( __FILE__ ) . 'templates/generic.html');
    $css = file_get_contents( plugin_dir_path( __FILE__ ) . 'templates/styles.css');
    
    $variables = [ 
      // text
      'body'    => $message, 
      'footer_copyright'  => self::getSetting('footer_copyright'),
      // colors
      'color_bg' => self::getSetting('color_bg'),
      'color_copyright' => self::getSetting('color_copyright'),
    ];
    
    // header image
    
    if ( self::getSetting('header_image_attachment_id') )
    {
      $image = wp_get_attachment_image_src( self::getSetting('header_image_attachment_id'), 'email_header' );
      $variables['header'] = '<a href="' . home_url( '/' ) . '"><img class="img-responsive" src="'  . @$image[0] . '"></a>';
    }
    
    // body header
    
    $variables['body_header'] = self::getSetting('header_content');
    
    // body footer
  
    $variables['body_footer'] = self::getSetting('footer_content');
    
    // do HTML mustache parsing
    $html = $this->doMustacheParsing($html, $variables);

    // do CSS mustache parsing
    $css = $this->doMustacheParsing($css, $variables);
    
    // add custom css
    
    $css .= "\n" . self::getSetting('custom_css');
    
    // emogrify
        
    $message = CssInliner::fromHtml($html)->inlineCss($css)->render();
    
    return $message;
  }
  
  public function previewTemplate( $message )
  {
    return $this->generateMailMessage( $message );
  }
  
}


