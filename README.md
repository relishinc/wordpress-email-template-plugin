# Email Template Plugin

A WordPress plugin that makes all emails sent by WP look a whole lot nicer (on all devices). You can customize the template by adding a header image, specifying colors, including custom CSS, and adding boilerplate content like a common header and footer. Wow!

### Installation

Grab the latest version here: 

https://bitbucket.org/relishinc/wordpress-email-templates-plugin/get/HEAD.zip

Unzip it and put it in your ```/wp-content/plugins``` folder. Activate the plugin and go. There's a settings page in the admin to customize the template and a preview page to see what your emails look like.

### Credits

The template HTML and CSS are based mostly on Lee Munroe's response HTML email template: https://github.com/leemunroe/responsive-html-email-template

